//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Web_Instituicao.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Pessoa
    {
        public int pessoaID { get; set; }

        [Display(Name = "Nome")]
        public string nome_sobrenome { get; set; }
        [Display(Name = "Cpf")]
        public string cpf { get; set; }
        [Display(Name = "Telefone")]
        public string telefone { get; set; }
        [Display(Name = "Email")]
        public string email { get; set; }
        [Display(Name = "Cargo")]
        public string cargo { get; set; }
        [Display(Name = "Usu�rio")]
        public string usuario { get; set; }
        [Display(Name = "Senha")]
        public string senha { get; set; }
        public int instituicaoID { get; set; }
        public int enderecoID { get; set; }
    
        public virtual Endereco Endereco { get; set; }
        public virtual Instituicao Instituicao { get; set; }
    }
}
