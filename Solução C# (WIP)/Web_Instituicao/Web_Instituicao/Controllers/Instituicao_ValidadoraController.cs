﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_Instituicao.Models;

namespace Web_Instituicao.Controllers
{
    [Authorize(Roles = "Superindente")]
    public class Instituicao_ValidadoraController : Controller
    {
        private Instituicao2Entities db = new Instituicao2Entities();

        // GET: Instituicao_Validadora
        public ActionResult Index()
        {
            var instituicao_Validadora = db.Instituicao_Validadora.Include(i => i.Instituicao);
            return View(instituicao_Validadora.ToList());
        }

        // GET: Instituicao_Validadora/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Instituicao_Validadora instituicao_Validadora = db.Instituicao_Validadora.Find(id);
            if (instituicao_Validadora == null)
            {
                return HttpNotFound();
            }
            return View(instituicao_Validadora);
        }

        // GET: Instituicao_Validadora/Create
        public ActionResult Create()
        {
            ViewBag.instituicaoID = new SelectList(db.Instituicaos, "instituicaoID", "nome");
            return View();
        }

        // POST: Instituicao_Validadora/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "InstituicaoValidadoraID,nome,mantenedora,credenciamentoMEC,instituicaoID")] Instituicao_Validadora instituicao_Validadora)
        {
            if (ModelState.IsValid)
            {
                db.Instituicao_Validadora.Add(instituicao_Validadora);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.instituicaoID = new SelectList(db.Instituicaos, "instituicaoID", "nome", instituicao_Validadora.instituicaoID);
            return View(instituicao_Validadora);
        }

        // GET: Instituicao_Validadora/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Instituicao_Validadora instituicao_Validadora = db.Instituicao_Validadora.Find(id);
            if (instituicao_Validadora == null)
            {
                return HttpNotFound();
            }
            ViewBag.instituicaoID = new SelectList(db.Instituicaos, "instituicaoID", "nome", instituicao_Validadora.instituicaoID);
            return View(instituicao_Validadora);
        }

        // POST: Instituicao_Validadora/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "InstituicaoValidadoraID,nome,mantenedora,credenciamentoMEC,instituicaoID")] Instituicao_Validadora instituicao_Validadora)
        {
            if (ModelState.IsValid)
            {
                db.Entry(instituicao_Validadora).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.instituicaoID = new SelectList(db.Instituicaos, "instituicaoID", "nome", instituicao_Validadora.instituicaoID);
            return View(instituicao_Validadora);
        }

        // GET: Instituicao_Validadora/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Instituicao_Validadora instituicao_Validadora = db.Instituicao_Validadora.Find(id);
            if (instituicao_Validadora == null)
            {
                return HttpNotFound();
            }
            return View(instituicao_Validadora);
        }

        // POST: Instituicao_Validadora/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Instituicao_Validadora instituicao_Validadora = db.Instituicao_Validadora.Find(id);
            db.Instituicao_Validadora.Remove(instituicao_Validadora);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
