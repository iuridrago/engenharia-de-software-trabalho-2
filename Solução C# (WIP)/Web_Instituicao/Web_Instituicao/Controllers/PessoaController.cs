﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Web_Instituicao.Models;

namespace Web_Instituicao.Controllers
{
    [Authorize(Roles = "Diretor")]
    public class PessoaController : Controller
    {
        private Instituicao2Entities db = new Instituicao2Entities();

        // GET: Pessoa
        public ActionResult Index()
        {
            var pessoas = db.Pessoas.Include(p => p.Endereco).Include(p => p.Instituicao);
            return View(pessoas.ToList());
        }

        // GET: Pessoa/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pessoa pessoa = db.Pessoas.Find(id);
            if (pessoa == null)
            {
                return HttpNotFound();
            }
            return View(pessoa);
        }

        // GET: Pessoa/Create
        public ActionResult Create()
        {
            ViewBag.enderecoID = new SelectList(db.Enderecoes, "enderecoID", "estado");
            ViewBag.instituicaoID = new SelectList(db.Instituicaos, "instituicaoID", "nome");
            return View();
        }

        // POST: Pessoa/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "pessoaID,nome_sobrenome,cpf,telefone,email,cargo,usuario,senha,instituicaoID,enderecoID")] Pessoa pessoa)
        {
            if (ModelState.IsValid)
            {
                db.Pessoas.Add(pessoa);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.enderecoID = new SelectList(db.Enderecoes, "enderecoID", "estado", pessoa.enderecoID);
            ViewBag.instituicaoID = new SelectList(db.Instituicaos, "instituicaoID", "nome", pessoa.instituicaoID);
            return View(pessoa);
        }

        // GET: Pessoa/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pessoa pessoa = db.Pessoas.Find(id);
            if (pessoa == null)
            {
                return HttpNotFound();
            }
            ViewBag.enderecoID = new SelectList(db.Enderecoes, "enderecoID", "estado", pessoa.enderecoID);
            ViewBag.instituicaoID = new SelectList(db.Instituicaos, "instituicaoID", "nome", pessoa.instituicaoID);
            return View(pessoa);
        }

        // POST: Pessoa/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pessoaID,nome_sobrenome,cpf,telefone,email,cargo,usuario,senha,instituicaoID,enderecoID")] Pessoa pessoa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pessoa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.enderecoID = new SelectList(db.Enderecoes, "enderecoID", "estado", pessoa.enderecoID);
            ViewBag.instituicaoID = new SelectList(db.Instituicaos, "instituicaoID", "nome", pessoa.instituicaoID);
            return View(pessoa);
        }

        // GET: Pessoa/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pessoa pessoa = db.Pessoas.Find(id);
            if (pessoa == null)
            {
                return HttpNotFound();
            }
            return View(pessoa);
        }

        // POST: Pessoa/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pessoa pessoa = db.Pessoas.Find(id);
            db.Pessoas.Remove(pessoa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
