﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Web_Instituicao.Models;

namespace Web_Instituicao.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Login(string returnURL)
        {
            /*Recebe a url que o usuário tentou acessar*/
            ViewBag.ReturnUrl = returnURL;
            return View(new Pessoa());
        }

        /// <param name = "login" ></ param >
        /// < param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Pessoa login, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                using (Instituicao2Entities db = new Instituicao2Entities())
                {
                    var vLogin = db.Pessoas.Where(p => p.usuario.Equals(login.usuario)).FirstOrDefault();
                    /*Verificar se a variavel vLogin está vazia.
                    Isso pode ocorrer caso o usuário não existe.
              Caso não exista ele vai cair na condição else.*/
                    if (vLogin != null)
                    {
                        /*Código abaixo verifica se a senha digitada no site é igual a
                        senha que está sendo retornada
                         do banco. Caso não, cai direto no else*/
                        if (Equals(vLogin.senha, login.senha))
                        {
                            FormsAuthentication.SetAuthCookie(vLogin.usuario, false);
                            if (Url.IsLocalUrl(returnUrl)
                            && returnUrl.Length > 1
                            && returnUrl.StartsWith("/")
                            && !returnUrl.StartsWith("//")
                            && returnUrl.StartsWith("/\\"))
                            {
                                return Redirect(returnUrl);
                            }

                                /*código abaixo cria uma session para armazenar o nome do usuário*/
                                Session["Nome"] = vLogin.nome_sobrenome;
                                //Session["Sobrenome"] = vLogin.SOBRENOME;
                                /*retorna para a tela inicial do Home*/
                                return RedirectToAction("Index", "Home");
                        }
                        /*Else responsável da validação da senha*/
                        else
                        {
                            /*Escreve na tela a mensagem de erro informada*/
                            ModelState.AddModelError("", "Senha informada Inválida!!!");
                            /*Retorna a tela de login*/
                            return View(new Pessoa());
                        }
                    }
                    /*Else responsável por verificar se o usuário existe*/
                    else
                    {
                        /*Escreve na tela a mensagem de erro informada*/
                        ModelState.AddModelError("", "E-mail informado inválido!!!");
                        /*Retorna a tela de login*/
                        return View(new Pessoa());
                    }
                }
            }
            /*Caso os campos não esteja de acordo com a solicitação retorna a tela de login
            com as mensagem dos campos*/
            return View(login);
        }
    }
}