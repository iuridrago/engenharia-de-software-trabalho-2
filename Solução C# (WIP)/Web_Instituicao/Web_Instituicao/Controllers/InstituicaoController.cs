﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_Instituicao.Models;

namespace Web_Instituicao.Controllers
{
    public class InstituicaoController : Controller
    {
        private Instituicao2Entities db = new Instituicao2Entities();

        // GET: Instituicao
        public ActionResult Index()
        {
            var instituicaos = db.Instituicaos.Include(i => i.Endereco);
            return View(instituicaos.ToList());
        }

        // GET: Instituicao/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Instituicao instituicao = db.Instituicaos.Find(id);
            if (instituicao == null)
            {
                return HttpNotFound();
            }
            return View(instituicao);
        }

        // GET: Instituicao/Create
        public ActionResult Create()
        {
            ViewBag.enderecoID = new SelectList(db.Enderecoes, "enderecoID", "cidade");
            return View();
        }

        // POST: Instituicao/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "instituicaoID,nome,mantenedora,credenciamentoMEC,enderecoID")] Instituicao instituicao)
        {
            if (ModelState.IsValid)
            {
                db.Instituicaos.Add(instituicao);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.enderecoID = new SelectList(db.Enderecoes, "enderecoID", "cidade", instituicao.enderecoID);
            return View(instituicao);
        }

        // GET: Instituicao/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Instituicao instituicao = db.Instituicaos.Find(id);
            if (instituicao == null)
            {
                return HttpNotFound();
            }
            ViewBag.enderecoID = new SelectList(db.Enderecoes, "enderecoID", "cidade", instituicao.enderecoID);
            return View(instituicao);
        }

        // POST: Instituicao/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "instituicaoID,nome,mantenedora,credenciamentoMEC,enderecoID")] Instituicao instituicao)
        {
            if (ModelState.IsValid)
            {
                db.Entry(instituicao).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.enderecoID = new SelectList(db.Enderecoes, "enderecoID", "cidade", instituicao.enderecoID);
            return View(instituicao);
        }

        // GET: Instituicao/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Instituicao instituicao = db.Instituicaos.Find(id);
            if (instituicao == null)
            {
                return HttpNotFound();
            }
            return View(instituicao);
        }

        // POST: Instituicao/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Instituicao instituicao = db.Instituicaos.Find(id);
            db.Instituicaos.Remove(instituicao);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
